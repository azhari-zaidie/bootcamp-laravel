@extends ('layout.public')
@section('content')
@if (session('status'))
<div>
    {{ session('status') }}
</div>
@endif

@if ($errors->any())
<div>
    <div>{{ __('Whoops! Something went wrong.') }}</div>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form method="POST" action="{{ route('login') }}" class="user">
    @csrf
    <div class="form-group">
        <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." required autofocus>
    </div>
    <div class="form-group">
        <input type="password" name="password" required autocomplete="current-password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
    </div>

    <div class="form-group">
        <div class="custom-control custom-checkbox small">
            <input type="checkbox" name="remember" class="custom-control-input" id="customCheck">
            <label class="custom-control-label" for="customCheck">Remember Me</label>
        </div>
    </div>

    <div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            {{ __('Login') }}
        </button>
    </div>
    <hr>
    <div class="text-center">
        @if (Route::has('password.request'))
        <a class="small" href="{{ route('password.request') }}">Forgot Password?</a>
        @endif
    </div>

    <div class="text-center">
        <a class="small" href="/register">Create an Account!</a>
    </div>
</form>
@endsection