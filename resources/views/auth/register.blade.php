@extends ('layout.public')

@section('content')
@if ($errors->any())
<div>
    <div>{{ __('Whoops! Something went wrong.') }}</div>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form method="POST" action="{{ route('register') }}" class="user">
    @csrf

    <div class="form-group row">
        <div class="col-sm-6 mb-3 mb-sm-0">
            <input type="text" name="name" value="{{ old('name') }}" required autofocus autocomplete="name" class="form-control form-control-user" id="exampleFirstName" placeholder="Name">
        </div>
        <div class="col-sm-6">
            <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-user" id="exampleLastName" placeholder="Email">
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-6 mb-3 mb-sm-0">
            <input type="password" name="password" required autocomplete="new-password" class="form-control form-control-user" placeholder="Password">
        </div>
        <div class="col-sm-6">
            <input type="password" name="password_confirmation" required autocomplete="new-password" class="form-control form-control-user" placeholder="Confirm Password">
        </div>
    </div>
    <div class="text-center">
        <a class="small" href="/login">Already have an Account? Login</a>
    </div>
    <hr>
    <div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            {{ __('Register') }}
        </button>
    </div>
</form>
@endsection