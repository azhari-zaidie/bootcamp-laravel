<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    //tujuan untuk benarkan column yang boleh diedit di database
    protected $fillable = [
        'name'
    ];
}
