<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class MemberController extends Controller
{
    // --- /member
    public function index()
    {
        return view('dashboard');
    }

    // --- /member/profile
    public function profile()
    {
        # code...
        return view('member.profile');
    }

    // --- /member/login
    public function login()
    {
        # code...
        return view('member.login');
    }

    // --- /member/register
    public function register()
    {
        return view('member.register');
    }
}
