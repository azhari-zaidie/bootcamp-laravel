<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Unique;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        # code...
        if (!Gate::allows('is-admin')) {
            abort(403);
        }
        $users = User::paginate(10);

        return view('admin.user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //simpan data
        $form_data = $request->validate(
            [
                'name' => 'required|min:5|max:255',
                'email' => 'required|email',
                'password' => 'required|confirmed',
            ]
        );

        $form_data['password'] = Hash::make($form_data['password']);

        $user = User::create($form_data);

        return redirect('admin/user')->with('success', "Pengguna {$user->name} ($user->id) telah ditambah");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::find($id);
        $roles = Role::all();

        return view('admin.user.edit', ['user' => $user, 'roles' => $roles]);

        //dd($user);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //

        $user = User::findOrFail($id);

        $form_data = $request->validate(
            [
                'name' => 'required|min:5|max:255',
                'email' => 'required|email',
            ]
        );
        $user->name = $form_data['name'];
        $user->email = $form_data['email'];

        $user->save();

        $user->roles()->sync($request->roles);

        return redirect('admin/user')->with('success', "Pengguna {$user->name} ($id) telah dikemaskini");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
