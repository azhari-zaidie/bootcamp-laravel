<?php

use App\Http\Controllers\MemberController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
	return view('main');
});

Route::get('/member', [MemberController::class, 'index']);

Route::get('/member/login', [MemberController::class, 'login']);

Route::get('/member/register', [MemberController::class, 'register']);

Route::get('/member/profile', [MemberController::class, 'profile']);

Route::get(
	'/dashboard',
	[MemberController::class, 'index']
)
	->name('dashboard')
	->middleware(['auth']);


// -- Admin User Module
Route::resource('admin/user', UserController::class);
Route::resource('admin/role', RoleController::class);
